const express = require("express");
const socketio = require("socket.io");
const http = require("http");
const PORT = process.env.PORT || 7000;
const cors = require("cors");
const app = express();
const server = http.createServer(app);
const io = socketio(server, {
  cors: {
    origin: "http://localhost:3000",
    methods: ["GET", "POST"],
  },
});
app.use(
  cors({
    origin: "http://localhost:3000",
    credentials: true,
  })
);

io.on("connection", (socket) => {
  socket.on("join", (data) => {
    io.emit("response", data);
  });
  socket.on("order", (data) => {
    io.emit("response", data);
  });
});

server.listen(PORT, () => {
  console.log("Connected Successfully");
});
