const mongoose = require("mongoose");
const IngredientSchema = mongoose.Schema(
  {
    name: {
      type: String,
      require: true,
    },
    category: {
      type: String,
      require: true,
    },
    unit: {
      type: String,
      require: true,
    },
    isDeleted: {
      type: Number,
      default: 0,
    },
    count: {
      type: Number,
      default: 0,
    },
    createdAt: {
      type: Date,
      default: Date.now,
    },
  },
  { versionKey: false }
);
const ingredientsModel = mongoose.model("Ingredients", IngredientSchema);
module.exports = ingredientsModel;
