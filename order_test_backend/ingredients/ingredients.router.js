const express = require("express");
const ingredientsModel = require("./ingredients.model");
const auth = require("../auth.js");
const ingredientsRouter = express.Router();

ingredientsRouter.post(`/create`, auth, (req, res) => {
  ingredientsModel
    .create({
      name: req.body.name,
      count: req.body.count,
      unit: "kg",
      category: req.body.category,
    })
    .then((data) => {
      return res.json({
        data: data,
      });
    })
    .catch((e) => {
      console.log(e);
    });
});

ingredientsRouter.post(`/getIngredientsPagintion`, auth, (req, res) => {
  let total = 0;
  ingredientsModel
    .countDocuments({ isDeleted: { $ne: 1 } })
    .then((data) => {
      total = data;
      return ingredientsModel
        .find({ isDeleted: { $ne: 1 } })
        .sort({ createdAt: -1 })
        .skip(req.body.limit * (req.body.skip - 1))
        .limit(req.body.limit);
    })
    .then((data1) => {
      return res.json({
        count: total,
        data: data1,
      });
    })
    .catch((e) => {
      console.log(e);
    });
});

ingredientsRouter.get(`/getById`, auth, (req, res) => {
  ingredientsModel
    .findOne({
      _id: req.query.id,
    })
    .then((data1) => {
      return res.json({
        data: data1,
      });
    })
    .catch((e) => {
      console.log(e);
    });
});

ingredientsRouter.post(`/update`, auth, (req, res) => {
  ingredientsModel
    .updateOne({ _id: req.body._id }, req.body)
    .then((data1) => {
      return res.json({
        success: true,
      });
    })
    .catch((e) => {
      console.log(e);
    });
});

module.exports = ingredientsRouter;
