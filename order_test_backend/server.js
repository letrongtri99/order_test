const express = require("express");
const bodyParser = require("body-parser");
const mongoose = require("mongoose");
const cors = require("cors");
const session = require("express-session");
const MongoStore = require("connect-mongo")(session);
const foodRouter = require("./food/food.router");
const ingredientsRouter = require("./ingredients/ingredients.router");
const orderRouter = require("./order/order.router");
const userRouter = require("./user/user.router");
const config = require("./config.js");
const db = mongoose.connection;

mongoose.connect(
  "mongodb://localhost:27017/order_test",
  { useNewUrlParser: true, useUnifiedTopology: true },
  (error) => {
    if (error) {
      console.log(error);
      throw error;
    } else {
      console.log("Connect to DB successfully");
      const app = express();

      app.use(bodyParser.json());

      app.use(
        session({
          secret: "somesecret",
          cookie: { maxAge: 24 * 60 * 60 * 1000 },
          store: new MongoStore({ mongooseConnection: db }),
        })
      );
      app.use(
        cors({
          origin: config.developer,
          credentials: true,
        })
      );
      app.use("/food", foodRouter);
      app.use("/ingredients", ingredientsRouter);
      app.use("/order", orderRouter);
      app.use("/user", userRouter);
      app.listen(3001, (err) => {
        if (err) {
          throw err;
        }
        console.log("Server listen on port 3001 ...");
      });
    }
  }
);
