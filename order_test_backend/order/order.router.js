const express = require("express");
const orderModel = require("./order.model");
const foodModel = require("../food/food.model");
const ingredientsModel = require("../ingredients/ingredients.model");
const auth = require("../auth.js");
const orderRouter = express.Router();
const app = express();

const STATUSES = {
  PENDING: 0,
  ACCEPTCOOKING: 1,
  REJECTCOOKING: 2,
  DONECOOKING: 3,
  OUTOFMATERIALS: 4,
};

orderRouter.post(`/create`, auth, (req, res) => {
  orderModel
    .create({
      food_id: req.body.id,
      count: req.body.count,
      status: STATUSES.PENDING,
    })
    .then((data) => {
      return res.json({
        data: data,
      });
    })
    .catch((e) => {
      console.log(e);
    });
});

orderRouter.get(`/getAllOrder`, auth, (req, res) => {
  orderModel
    .find({
      isDeleted: { $ne: 1 },
    })
    .sort({ createdAt: -1 })
    .populate("food_id", "name category imageUrl")
    .then((data) => {
      return res.json({
        data: data,
      });
    })
    .catch((e) => {
      console.log(e);
    });
});

orderRouter.post(`/updateDelete`, auth, (req, res) => {
  orderModel
    .updateOne({ _id: req.body.id }, { isDeleted: 1 })
    .then((data) => {
      return res.json({
        message: "success",
      });
    })
    .catch((e) => {
      console.log(e);
    });
});

orderRouter.get(`/checkMaterials`, auth, (req, res) => {
  let listMaterials = [];
  let listObjectIng = {};
  let listIds = [];
  let number = 0;
  let check = true;
  orderModel
    .findOne({
      _id: req.query.id,
      isDeleted: { $ne: 1 },
    })
    .then((data1) => {
      number = data1.count;
      return foodModel.findOne({
        _id: data1.food_id,
      });
    })
    .then((data2) => {
      listObjectIng = data2.ingredients[0];
      for (var key in listObjectIng) {
        listIds.push(key);
      }
      const listFindIng = [];

      listIds.forEach((el) => {
        listFindIng.push(ingredientsModel.findOne({ _id: el }));
      });

      return Promise.all(listFindIng);
    })
    .then((data3) => {
      data3.forEach((e) => {
        if (e.count < listObjectIng[e._id] * number) {
          check = false;
          listMaterials.push({
            name: e.name,
            unit: e.unit,
            count: listObjectIng[e._id] * number,
            status: "không đủ",
          });
        } else {
          listMaterials.push({
            name: e.name,
            unit: e.unit,
            count: listObjectIng[e._id] * number,
            status: "đủ",
          });
        }
      });
      return;
    })
    .then((data4) => {
      return res.json({
        check: check,
        list: listMaterials,
      });
    })
    .catch((e) => {
      console.log(e);
    });
});

orderRouter.get(`/doing`, auth, (req, res) => {
  let listObjectIng = {};
  let listIds = [];
  let number = 0;
  orderModel
    .findOne({
      _id: req.query.id,
      isDeleted: { $ne: 1 },
    })
    .then((data1) => {
      number = data1.count;
      return foodModel.findOne({
        _id: data1.food_id,
      });
    })
    .then((data2) => {
      listObjectIng = data2.ingredients[0];
      for (var key in listObjectIng) {
        listIds.push(key);
      }
      const listFindIng = [];

      listIds.forEach((el) => {
        listFindIng.push(ingredientsModel.findOne({ _id: el }));
      });

      return Promise.all(listFindIng);
    })
    .then((data3) => {
      listUpdate = [];
      data3.forEach((e) => {
        listUpdate.push(
          ingredientsModel.updateOne(
            { _id: e._id },
            {
              count:
                parseInt(e.count) - parseInt(listObjectIng[e._id] * number),
            }
          )
        );
      });
      return Promise.all(listUpdate);
    })
    .then((data4) => {
      return orderModel.updateOne({ _id: req.query.id }, { status: 1 });
    })
    .then((data5) => {
      return res.json({
        message: "success",
      });
    })
    .catch((e) => {
      console.log(e);
    });
});

orderRouter.get(`/outOfMaterials`, auth, (req, res) => {
  orderModel
    .updateOne(
      {
        _id: req.query.id,
      },
      { status: 4 }
    )
    .then((data1) => {
      return res.json({
        message: "success",
      });
    })
    .catch((e) => {
      console.log(e);
    });
});

orderRouter.get(`/complete`, auth, (req, res) => {
  orderModel
    .updateOne(
      {
        _id: req.query.id,
      },
      { status: 3 }
    )
    .then((data1) => {
      return res.json({
        message: "success",
      });
    })
    .catch((e) => {
      console.log(e);
    });
});

module.exports = orderRouter;
