const mongoose = require("mongoose");
const OrderSchema = mongoose.Schema(
  {
    food_id: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Food",
      require: true,
    },
    count: {
      type: Number,
      require: true,
    },
    status: {
      type: Number,
      require: true,
    },
    isDeleted: {
      type: Number,
      default: 0,
    },
    createdAt: {
      type: Date,
      default: Date.now,
    },
  },
  { versionKey: false }
);
const orderModel = mongoose.model("Order", OrderSchema);
module.exports = orderModel;
