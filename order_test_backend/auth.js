module.exports = (req, res, next) => {
  if (req.session.currentUser) {
    return next();
  } else {
    return res.json({
      status: 401,
      message: "Unauthorized",
    });
  }
};
