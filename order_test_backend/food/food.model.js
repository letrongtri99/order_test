const mongoose = require("mongoose");
const FoodSchema = mongoose.Schema(
  {
    name: {
      type: String,
      require: true,
    },
    category: {
      type: String,
      require: true,
    },
    ingredients: {
      type: Array,
      require: true,
    },
    imageUrl: {
      type: String,
      requre: true,
    },
    isDeleted: {
      type: Number,
      default: 0,
    },
  },
  { versionKey: false }
);
const foodModel = mongoose.model("Food", FoodSchema);

module.exports = foodModel;
