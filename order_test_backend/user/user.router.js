const express = require("express");
const userModel = require("./user.model");
const userRouter = express.Router();
const bcryptjs = require("bcryptjs");
const auth = require("../auth.js");
const app = express();

userRouter.post(`/create`, auth, (req, res) => {
  const hashPassword = bcryptjs.hashSync("123456a@", 10);
  userModel
    .create({
      email: "trilt",
      password: hashPassword,
    })
    .then((data) => {
      return res.json({
        data: data,
      });
    })
    .catch((e) => {
      console.log(e);
    });
});

userRouter.post(`/login`, (req, res) => {
  userModel
    .findOne({ email: req.body.email })
    .then((data1) => {
      if (!data1) {
        res.json({
          success: false,
          message: "Email không tồn tại",
        });
      } else if (!bcryptjs.compareSync(req.body.password, data1.password)) {
        res.json({
          success: false,
          message: "Sai mật khẩu",
        });
      } else {
        req.session.currentUser = {
          id: data1._id,
          email: data1.email,
        };
        res.json({
          success: true,
          message: "Đăng nhập thành công",
        });
      }
    })
    .catch((e) => {
      console.log(e);
    });
});

userRouter.get(`/checkSession`, (req, res) => {
  if (req.session.currentUser) {
    return res.json({
      success: true,
      data: req.session.currentUser,
    });
  } else {
    return res.json({
      success: false,
      message: "No session",
    });
  }
});

userRouter.get(`/logout`, auth, (req, res) => {
  req.session.destroy((err) => {
    if (err) {
      return res.json({
        success: false,
        message: "Đăng xuất không thành công",
      });
    } else {
      return res.json({
        success: true,
        message: "Đăng xuất thành công",
      });
    }
  });
});

module.exports = userRouter;
