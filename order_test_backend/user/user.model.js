const mongoose = require("mongoose");
const UserSchema = mongoose.Schema(
  {
    email: {
      type: String,
      require: true,
    },
    password: {
      type: String,
      require: true,
    },
    isDeleted: {
      type: Number,
      default: 0,
    },
  },
  { versionKey: false }
);
const userModel = mongoose.model("User", UserSchema);
module.exports = userModel;
