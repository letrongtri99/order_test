import React, { useEffect, useState } from "react";
import LayoutAll from "../Layout";
import {
  Layout,
  Tooltip,
  Button,
  Card,
  Modal,
  Row,
  Col,
  Image,
  InputNumber,
  Typography,
  Spin,
  List,
  Tabs,
  Descriptions,
  message,
} from "antd";
import io from "socket.io-client";
import TableMaterial from "./TableMaterial";
import Config from "../../config";

import { ExclamationCircleOutlined } from "@ant-design/icons";

const { Header, Content, Footer, Sider } = Layout;
const { Meta } = Card;
const { Title, Text } = Typography;
const { confirm } = Modal;

const socket = io(`${Config.chatbot}`);
const { TabPane } = Tabs;

function callback(key) {
  console.log(key);
}

const KitchenView = () => {
  const [listOrder, setListOrder] = useState([]);
  const [loading, setLoading] = useState(false);
  const [visible, setVisible] = useState(false);
  const [listMaterial, setListMaterial] = useState([]);
  const [checkEnough, setCheckEnough] = useState(true);
  const [idItem, setIdItem] = useState("");
  const [confirmLoading, setConfirmLoading] = useState(false);

  const showDeleteConfirm = (id) => {
    confirm({
      title: "Bạn có chắc chắn muốn hủy order này?",
      icon: <ExclamationCircleOutlined />,
      okText: "Đồng ý",
      okType: "danger",
      cancelText: "Từ chối",
      onOk() {
        fetch(`${Config.developer}/order/updateDelete`, {
          method: "POST",
          credentials: "include",
          headers: {
            "Content-Type": "application/json",
          },
          body: JSON.stringify({
            id: id,
          }),
        })
          .then((res) => res.json())
          .then((data) => {
            message.success("Hủy thành công");
            socket.emit("order");
          })
          .catch((e) => {
            console.log(e);
          });
      },
      onCancel() {},
    });
  };

  const getAllOrder = () => {
    setLoading(true);
    fetch(`${Config.developer}/order/getAllOrder`, {
      method: "GET",
      credentials: "include",
      headers: {
        "Content-Type": "application/json",
      },
    })
      .then((res) => res.json())
      .then((data) => {
        const listPush = [];
        data.data.forEach((e) => {
          listPush.push({
            id: e._id,
            name: e.food_id.name,
            imageUrl: e.food_id.imageUrl,
            count: e.count,
            status: e.status,
          });
        });
        setListOrder(listPush);
        setLoading(false);
      })
      .catch((e) => {
        console.log(e);
      });
  };

  const showModalMaterial = (id) => {
    fetch(`${Config.developer}/order/checkMaterials?id=${id}`, {
      method: "GET",
      credentials: "include",
      headers: {
        "Content-Type": "application/json",
      },
    })
      .then((res) => res.json())
      .then((data) => {
        setIdItem(id);
        setCheckEnough(data.check);
        setListMaterial(data.list);
        setVisible(true);
      })
      .catch((e) => {
        console.log(e);
      });
  };

  const handleOk = () => {
    setConfirmLoading(true);
    if (checkEnough == true) {
      fetch(`${Config.developer}/order/doing?id=${idItem}`, {
        method: "GET",
        credentials: "include",
        headers: {
          "Content-Type": "application/json",
        },
      })
        .then((res) => res.json())
        .then((data) => {
          socket.emit("order");
          setConfirmLoading(false);
          setVisible(false);
          message.success("Xác nhận thành công");
        })
        .catch((e) => {
          console.log(e);
        });
    } else {
      fetch(`${Config.developer}/order/outOfMaterials?id=${idItem}`, {
        method: "GET",
        credentials: "include",
        headers: {
          "Content-Type": "application/json",
        },
      })
        .then((res) => res.json())
        .then((data) => {
          socket.emit("order");
          setConfirmLoading(false);
          setVisible(false);
          message.success("Xác nhận thành công");
        })
        .catch((e) => {
          console.log(e);
        });
    }
  };
  const completeOrder = (id) => {
    fetch(`${Config.developer}/order/complete?id=${id}`, {
      method: "GET",
      credentials: "include",
      headers: {
        "Content-Type": "application/json",
      },
    })
      .then((res) => res.json())
      .then((data) => {
        socket.emit("order");
        message.success("Xác nhận thành công");
      })
      .catch((e) => {
        console.log(e);
      });
  };

  const handleCancel = () => {
    setVisible(false);
  };
  useEffect(() => {
    socket.emit("join");
    socket.on("response", (res) => {
      getAllOrder();
    });
  }, []);
  return (
    <LayoutAll>
      <Modal
        title="Tình trạng nguyên liệu"
        visible={visible}
        onOk={handleOk}
        confirmLoading={confirmLoading}
        onCancel={handleCancel}
        okText={
          checkEnough == true ? "Xác nhận chế biến" : "Báo hết nguyên liệu"
        }
        cancelText="Đóng"
      >
        <Descriptions layout="vertical" bordered>
          <Descriptions.Item label="Kết quả kiểm tra tình trạng nguyên liệu cần có cho order">
            {listMaterial.map((e) => {
              return (
                <div>
                  {e.name} : {e.count} {e.unit} ({e.status})
                  <br />
                </div>
              );
            })}
            <br />
            {checkEnough == false
              ? "Nguyên liệu không đủ ( cần thêm nguyên liệu )"
              : "Nguyên liêu đủ"}
          </Descriptions.Item>
        </Descriptions>
      </Modal>
      <Content
        style={{
          padding: 24,
          margin: 0,
          minHeight: "80vh",
          background: "#fff",
        }}
      >
        <Tabs
          defaultActiveKey="1"
          onChange={callback}
          style={{ paddingLeft: "50px" }}
        >
          <TabPane tab="Order" key="1">
            {loading == true ? (
              <div className="demo-loading-container">
                <Spin style={{ fontSize: "55px" }} />
              </div>
            ) : (
              <List
                dataSource={listOrder}
                style={{ width: "100%" }}
                grid={{ column: 3 }}
                renderItem={(item) => (
                  <List.Item style={{ margin: "5px" }}>
                    <Row
                      style={{
                        padding: "15px",
                        width: "100%",
                        border: "0.5px solid rgb(237 231 231)",
                      }}
                    >
                      <Col span={12}>
                        <img width={160} height={100} src={item.imageUrl} />
                      </Col>
                      <Col span={12}>
                        <Row>
                          <Col span={24}>
                            <Title
                              level={5}
                              style={{ float: "right", marginRight: "10px" }}
                            >
                              {item.count} {item.name}
                            </Title>
                          </Col>
                        </Row>
                        <Row>
                          <Col span={24}>
                            <Text
                              style={{ float: "right", marginRight: "10px" }}
                            >
                              Trạng thái:
                              {item.status == 0
                                ? " Chờ xác nhận"
                                : item.status == 1
                                ? " Đầu bếp đang chế biến"
                                : item.status == 2
                                ? " Đầu bếp từ chối"
                                : item.status == 3
                                ? " Đầu bếp hoàn thành"
                                : item.status == 4
                                ? " Hết nguyên liệu"
                                : ""}
                            </Text>
                          </Col>
                        </Row>
                        <Row style={{ marginTop: "10px" }}>
                          <Col span={24}>
                            {item.status == 0 ||
                            item.status == 2 ||
                            item.status == 4 ? (
                              <div>
                                <Button
                                  type="primary"
                                  size="small"
                                  danger
                                  onClick={() => {
                                    showDeleteConfirm(item.id);
                                  }}
                                >
                                  Hủy
                                </Button>
                                <Button
                                  type="primary"
                                  size="small"
                                  style={{
                                    float: "right",
                                    marginRight: "10px",
                                  }}
                                  onClick={() => {
                                    showModalMaterial(item.id);
                                  }}
                                >
                                  Kiểm tra nguyên liệu
                                </Button>
                              </div>
                            ) : item.status == 1 ? (
                              <Button
                                type="primary"
                                size="small"
                                style={{
                                  float: "right",
                                  marginRight: "10px",
                                  backgroundColor: "green",
                                }}
                                onClick={() => {
                                  completeOrder(item.id);
                                }}
                              >
                                Xác nhận hoàn thành
                              </Button>
                            ) : (
                              <div></div>
                            )}
                          </Col>
                        </Row>
                      </Col>
                    </Row>
                  </List.Item>
                )}
              ></List>
            )}
          </TabPane>
          <TabPane tab="Nguyên liệu" key="2">
            <TableMaterial />
          </TabPane>
        </Tabs>
      </Content>
    </LayoutAll>
  );
};

export default KitchenView;
