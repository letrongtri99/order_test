import React, { useState, useEffect } from "react";
import { BrowserRouter, Switch, Route, Link } from "react-router-dom";
import { useHistory, Redirect } from "react-router-dom";
import OrderView from "./Order/OrderView";
import KitchenView from "./Kitchen/KitchenView";
import LoginView from "./Login/LoginView";
import { Spin, Space } from "antd";
import Config from "../config";

const RootView = () => {
  function PrivateRoute({ children, ...rest }) {
    const [state, setState] = useState("loading");
    useEffect(() => {
      fetch(`${Config.developer}/user/checkSession`, {
        method: "GET",
        credentials: "include",
        headers: {
          "Content-Type": "application/json",
        },
      })
        .then((res) => res.json())
        .then((data) => {
          if (data.success == true) {
            setState("isLoggedIn");
          } else {
            setState("notLoggedIn");
          }
        })
        .catch((e) => {
          console.log(e);
        });
    }, []);

    if (state === "loading") {
      return (
        <div className="example">
          <Spin />
        </div>
      );
    }
    return (
      <Route
        {...rest}
        render={({ location }) =>
          state === "isLoggedIn" ? (
            children
          ) : (
            <Redirect
              to={{
                pathname: "/",
                state: { from: location },
              }}
            />
          )
        }
      />
    );
  }

  function PublicRoute({ children, ...rest }) {
    const [state, setState] = useState("loading");
    useEffect(() => {
      fetch(`${Config.developer}/user/checkSession`, {
        method: "GET",
        credentials: "include",
        headers: {
          "Content-Type": "application/json",
        },
      })
        .then((res) => res.json())
        .then((data) => {
          if (data.success == true) {
            setState("isLoggedIn");
          } else {
            setState("notLoggedIn");
          }
        })
        .catch((e) => {
          console.log(e);
        });
    }, []);

    if (state === "loading") {
      return (
        <div className="example">
          <Spin />
        </div>
      );
    }
    return (
      <Route
        {...rest}
        render={({ location }) =>
          state === "notLoggedIn" ? (
            children
          ) : (
            <Redirect
              to={{
                pathname: "/order",
                state: { from: location },
              }}
            />
          )
        }
      />
    );
  }
  return (
    <BrowserRouter>
      <Switch>
        <PublicRoute exact path="/">
          <LoginView />
        </PublicRoute>
        <PrivateRoute exact path="/order">
          <OrderView />
        </PrivateRoute>
        <PrivateRoute exact path="/kitchen">
          <KitchenView />
        </PrivateRoute>
      </Switch>
    </BrowserRouter>
  );
};

export default RootView;
