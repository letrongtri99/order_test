import "./App.css";
import { Button } from "antd";
import RootView from "./View/RootView";
import OrderView from "./View/Order/OrderView";

function App() {
  return <RootView />;
}

export default App;
